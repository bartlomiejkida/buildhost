FROM fedora:latest

RUN adduser build && groupadd mock && usermod -a -G mock build
RUN dnf install -y \
    cmake \
    gcc-c++ \ 
    git \
    make \
    mock \
    python3 \
    python3-devel \
    python3-pip \
    rpmdevtools \
    rpmlint && \
    dnf clean all

USER build
